import {Row, Col,Card, Button, Container, Table} from 'react-bootstrap';
import {useState, useEffect, Fragment} from 'react';
import {Link, useNavigate, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import '../App.css';


export default function AllProductsTable(prop){

	const navigate = useNavigate();
	const {productId} = useParams();

const[productData, setProductData] = useState(null);
const[isActive, setIsActive] = useState(true);

const archive = (_id) =>{
      fetch(`${process.env.REACT_APP_URL}/products/archiveOrUnarchiveProduct/${_id}`, {
			method: "PATCH",
			headers:{
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body :  JSON.stringify({
			isActive : false
		})
			
		})
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          if (data === 'archive') {
          	
          	// console.log(productData);
          	productData.map(prod =>{

          			if (prod._id === _id) {
          				prod.isActive = false;
          			}

          	}) ;
            Swal.fire({
              title: "Product Successfully Archived",
              icon: "success",
              text: "Product deactivated",
            });

            navigate("/dashboard")
          }

          else {
            Swal.fire({
              title: "Product Archived Failed",
              icon: "error",
              text: "Failure to deactivate the product, please try again.",
            });
          }
        });
    }


const unarchive = (_id) =>{
      fetch(`${process.env.REACT_APP_URL}/products/archiveOrUnarchiveProduct/${_id}`, {
			method: "PATCH",
			headers:{
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body :  JSON.stringify({
			isActive : true
		})
			
		})
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          if (data === 'unarchive') {

          	productData.map(prod =>{

          			if (prod._id === _id) {
          				prod.isActive = true;
          			}

          	}) ;


            Swal.fire({
              title: "Product Successfully Unarchive",
              icon: "success",
              text: "Product activated",
            });

            navigate("/dashboard")
          }

          else {
            Swal.fire({
              title: "Product UnArchived Failed",
              icon: "error",
              text: "Failure to activate the product, please try again.",
            });
          }
        });
    }




	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/products/allProducts`,{
			headers: {
			// "Content-Type" : "application/json",
			Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(response => response.json())
		.then(data =>{
			// console.log("here");
			setProductData(data);

		}).catch((err) => {
			console.log(err.message);
		})
	},[])

	return(
		<Container fluid>
		<div>
			<div className = "mt-4">
				<div className = "card-title text-center mt-4">
					<h2> Admin Dashboard</h2>
				</div>

				<div>
					<div>
						<Link className = "btn btn-primary mb-2 p-3" to = "/addProduct"> Add New Product (+)</Link>
					</div>


					<table className = "table table-bordered"  responsive="sm">
						<thead className = "bg-dark text-white d-none d-md-table-header-group text-center">
							<tr>
								<th className="d-none d-md-table-cell">ID</th>
								<th className="d-none d-md-table-cell">Product Name</th>
								<th className="d-none d-md-table-cell">Description</th>
								<th className="d-none d-md-table-cell">Price</th>
								<th className="d-none d-md-table-cell">Date Created</th>
								<th className="d-none d-md-table-cell">Availability</th>
								<th className="d-none d-md-table-cell">Action</th>
								<th className="d-none d-md-table-cell">Update Products</th>
							</tr>
						</thead>

						<tbody>

							{
								productData &&
								productData.map(item => (
									<tr key={item._id}>
										<td className="d-none d-md-table-cell">{item._id}</td>
										<td className="d-none d-md-table-cell">{item.productName}</td>
										<td className="d-none d-md-table-cell">{item.description}</td>
										<td className="d-none d-md-table-cell">{item.price}</td>
										<td className="d-none d-md-table-cell">{item.createdOn}</td>
										{/*<td className="d-none d-md-table-cell">{item.isActive}</td>*/}
										{
											(item.isActive === true	)?	
											<td className="d-none d-md-table-cell">Available</td>
											:
											<td className="d-none d-md-table-cell">Not Available</td>
										}
										{
											(item.isActive === true	)?
											<td><Button onClick ={() =>{archive(item._id)}} className = "btn btn-danger tablebtn btnisActive"> archive</Button></td>
											:
											<td><Button onClick ={() =>{unarchive(item._id)}} className = "btn btn-primary tablebtn w-5 btnisActive"> unarchive</Button></td>
										}

										<td className="d-none d-md-table-cell"><Button as={Link} to={`/products/updateProduct/${item._id}`} className = "btn btn-success tablebtn">Update</Button>
								
										</td>
									</tr>

									))
							}
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
		</Container>

	);

}