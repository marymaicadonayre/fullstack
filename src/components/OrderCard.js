import {Row, Col,Card, Button, Container, CardImg} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import '../App.css';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';




export default function OrderCard(prop){


const history = useNavigate();
const {_id, userId, productId, productName, quantity, totalAmount, purchasedOn} = prop.prop;



const {user} = useContext(UserContext);

const checkOut = () => {
	console.log(userId)

	fetch(`${process.env.REACT_APP_URL}/orders/checkOut`,{
	method: "POST",
	headers: {
		"Content-Type" : "application/json",
		Authorization: `Bearer ${localStorage.getItem('token')}`
	},
	body:JSON.stringify({
		cartId:_id
	})
	})
	.then(response => response.json())
		.then(data =>{
			Swal.fire({
		        title: "checkOut Successfully!",
		        icon: "success",
		      })
		history("/products");
		})
	}



	return(
		<Container>
		<Row>
			<Col xs = {12} md = {6} className = "offset-md-3 offset-0 mt-2">
				<Card>
					{/*<CardImg top width="100%" src="../../images/ndress.jpeg" alt="ndress"/>*/}
				    <Card.Body className = "text-center">
				    	<Card.Subtitle className = "fw-bold">Order ID:</Card.Subtitle>
				        <Card.Text>
				        	{_id}
				        </Card.Text>

				        <Card.Subtitle className = "fw-bold">Product Name:</Card.Subtitle>
				        <Card.Text>
				        	{productName}
				        </Card.Text>

				        <Card.Subtitle className = "fw-bold">Quantity:</Card.Subtitle>
				        <Card.Text>
				        	{quantity}
				        </Card.Text>

				        <Card.Subtitle className = "fw-bold">Total Amount:</Card.Subtitle>
				        <Card.Text>
				        	P {totalAmount}
				        </Card.Text>

				        <Card.Subtitle className = "fw-bold">Puchased Date:</Card.Subtitle>
				        <Card.Text>
				        	{purchasedOn}
				        </Card.Text>
				        {/*<Button variant="primary">See Details</Button>*/}

				        <Button onClick = {checkOut} variant="primary" className = "prodviewbtn">Check Out</Button>
        		        
				    </Card.Body>
				</Card>
			</Col>
		</Row>
		</Container>

	)

}