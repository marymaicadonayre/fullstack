import {Row, Col,Card, Button, Container, CardImg} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';
import '../App.css';
import UserContext from '../UserContext';



export default function ProductCard(prop){

const {_id, productName, description, price, isActive} = prop.prop;

const {user} = useContext(UserContext);



	return(
		<Container>
		<Row>
			<Col xs = {12} md = {6} className = "offset-md-3 offset-0 mt-2">
				<Card>
					{/*<Card.Img variant="top" src="holder.js/100px180"/>*/}
				    <Card.Body className = "text-center">
				    	<Card.Title>{productName}</Card.Title>

				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				        	{description}
				        </Card.Text>

				        <Card.Subtitle className = "fw-bold">Price:</Card.Subtitle>
				        <Card.Text>
				        	P {price}
				        </Card.Text>
				        {/*<Button variant="primary">See Details</Button>*/}

				        {
				        	(user !== null)? 
				        	<Button  as = {Link} to = {`/products/${_id}`} variant="primary" className = "prodviewbtn">Details</Button>

				        	:

				        	<Button as = {Link} to = "/login" variant="primary" className = "prodviewbtn">mao ni</Button>

				        }
		        		        
				    </Card.Body>
				</Card>
			</Col>
		</Row>
		</Container>

	)

}