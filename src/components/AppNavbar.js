
import {Fragment, useContext} from 'react';

//importing modules using deconstruct
import {Container, Nav, Navbar} from 'react-bootstrap';

import {NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import '../App.css';

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
		<Navbar expand="lg" className = "NavMain"  fixed="top">
		    <Container fluid className = "nav">
		        <Navbar.Brand as = {NavLink} to =  "/"><span className= "mmlogo">M&M</span>
		        <span className= "apparellogo">APPAREL</span>
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">	

		          	<Nav className="ms-auto NavOption">
			            
			            {
			            	(user.id !== null)?

			            	(user.isAdmin === true) ? 

			            	<Fragment>
			            		<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/dashboard">Dashboard</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
			            	</Fragment>
			            	:
			            	<Fragment>
			            	<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            	<Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>
			            	<Nav.Link as = {NavLink} to = "/orders">Cart</Nav.Link>
			            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
			            	</Fragment>
			            	:
			            	<Fragment>
			            		<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/register">Sign Up</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
			            	</Fragment>
			            }


		          	</Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)
}