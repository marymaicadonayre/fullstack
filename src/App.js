//pages import
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/Page404';
import Dashboard from './pages/Dashboard';
import AddProduct from './pages/AddProduct';
import UpdateProducts from './pages/UpdateProducts';
import ProductView from './pages/ProductView';
import Order from './pages/Order';
// import ArchiveUnarchhive from './pages/ArchiveUnarchhive';

import {useState, useEffect} from 'react';
// import {Fragment} from 'react';
import{BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';


import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({id: null, isAdmin: false});

  //function for clearing localstorage
  const unSetUser = () => {
    localStorage.clear();
  }

  /*useEffect(() =>{
    console.log(user);

  },[user]);*/

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_URL}/users/profile`,
      {
        headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }})
      .then(response => response.json())
      .then(data => {
        // console.log(data);

        setUser({id: data._id, isAdmin: data.isAdmin})
      })
    },[])


  return (
    /*Router/BrowserRouter > Routes > Route*/

    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <AppNavbar/>
          <Routes>
            <Route path = "/" element = {<Home/>}/>
            <Route path = "/products" element = {<Products/>}/>
            {/*<Route path = "/products/archiveOrUnarchiveProduct/:productId" element = {<ArchiveUnarchhive/>}/>*/}
            <Route path = "/products/updateProduct/:productId" element = {<UpdateProducts/>}/>
            <Route path = "/products/:productId" element = {<ProductView/>}/>
            <Route path = "/login" element = {<Login/>}/> 
            <Route path = "/orders" element = {<Order/>}/> 
            <Route path = "/register" element = {<Register/>} />
            <Route path = "/logout" element = {<Logout/>} />
            <Route path = "/dashboard" element = {<Dashboard/>} />
            <Route path = "/addProduct" element = {<AddProduct/>} />
            <Route path = "*" element = {<Page404/>} />
          </Routes>
      </Router>
    </UserProvider>
    
      
  );
}

export default App;
