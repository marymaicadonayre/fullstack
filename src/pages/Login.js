import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../App.css';



export default function Login(){

const [email, setEmail] = useState ('');
const [password, setPassword] = useState ('');
const [isActive, setIsActive] = useState(false);

const {user, setUser} = useContext(UserContext);

useEffect(() => {
	if(email !== "" && password !== ""){	
		setIsActive(true);
	}else{
		setIsActive(false);
	}
}, [email, password]);



function loginUser(event){
	event.preventDefault()

	
	fetch(`${process.env.REACT_APP_URL}/users/login`,{
		method: "POST",
		headers:{
			'Content-Type' : 'application/json',
		},
		body : JSON.stringify({
			email: email,
			password: password
		})
		
	}).then(response => response.json())
	.then(data => {
		// console.log(data);

		if (data.accessToken !== "empty") {
			localStorage.setItem('token', data.accessToken);
			retrieveUserDetails(data.accessToken);

		Swal.fire({
			title: "Login Successful",
			icon: "success",
			text: "Welcome to our website!"
		})
		}else{

			Swal.fire({
				title: "Aunthentication failed!",
				icon: "error",
				text: "Check you Login details and Try Again."
			})
			setPassword('');
		}
	})

	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_URL}/users/profile`,
			{
				headers: {
				Authorization: `Bearer ${token}`
			}})
			.then(response => response.json())
			.then(data => {
				// console.log(data);

				setUser({id: data._id, isAdmin: data.isAdmin})
			})
	}

}



	return (
		(user.id !== null) ?
			<Navigate to = "/" />:

		<Container>
		<Row>
			<Col className = "loginform col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {loginUser} className = 'p-3'>
					<h1 className = "mb-4 fw-bold"> Login </h1>
				  	<Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label className= "fw-bold">Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value = {email}
				    	onChange = {event => setEmail(event.target.value)}
				    	required/>
				  	</Form.Group>

					<Form.Group className="mb-3" controlId="password" >
					    <Form.Label className= "fw-bold">Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange = {event => setPassword(event.target.value)}
					    	required
					    	className = "mb-3"
					    	/>
					 	<Form.Text className="text-muted">
				        No account yet? 
				        <Link to = "/register">  Register</Link>
				        </Form.Text>
					</Form.Group>
				  	<Button variant="success" type="submit" disabled= {!isActive} className= "loginbtn">
				    Login
				  	</Button>

				</Form>
			</Col>

		</Row>
	    </Container>
	)
}