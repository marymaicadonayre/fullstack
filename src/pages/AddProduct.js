import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';

//we use this to get the input of the user
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import{useNavigate, Link} from 'react-router-dom';

import '../App.css';


export default function AddProduct(){

//state hooks to store the values of the input field from our user
const [productName, setProductName]  = useState ('');
const [description, setDescription]  = useState ('');
const [price, setPrice] = useState ('');

const history = useNavigate();
const{user,setUser} = useContext(UserContext);

	function addProduct(event){
	event.preventDefault()

		let addprod = {productName,description,price};
		// console.log(addprod);

		fetch(`${process.env.REACT_APP_URL}/products/addProduct`,{
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body : JSON.stringify(addprod)
			
		})
		.then(response => response.json())
		.then(data => {

			console.log(data);
			if (data) {
			
			Swal.fire({
				title: "Product Added Succefully",
				icon: "success",
				// text: "Please Login!"
			})

			history("/dashboard");

			}else{

			Swal.fire({
				title: "There seems an Error!",
				icon: "error",
				text: "Please Try Again."
			})	
			}
		
		})
	
	}

	


	return (
		<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {addProduct} className = 'p-3'>

					<h4 className="mb-3 mt-4">Create Products:</h4>
					<Form.Group className="mb-3" controlId="productname">
					    <Form.Label className= "fw-bold">Product Name:</Form.Label>
					    <Form.Control 
					    type = "text"
					    placeholder="Enter Product Name" 
					    value = {productName}
					    onChange = {event => setProductName(event.target.value)}
				    	required/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="description">
					    <Form.Label className= "fw-bold">Description:</Form.Label>
					    <Form.Control 
					    type = "text"
					    placeholder="Enter Product description" 
					    value = {description}
					    onChange = {event => setDescription(event.target.value)}
				    	required
				    	className = "descriptionBox"/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="price">
					    <Form.Label className= "fw-bold">Price:</Form.Label>
					    <Form.Control 
					    type = "number"
					    placeholder="Enter Product Price" 
					    value = {price}
					    onChange = {event => setPrice(event.target.value)}
				    	required/>
					</Form.Group>
				  
				  <Button variant="success" type="submit" className = "btnCreateProd">
				    Add Product
				  </Button>

				  <Button as = {Link} to = "/dashboard" variant="danger" type="submit" className = "btnCreateProd">
				    Back
				  </Button>

				</Form>
			</Col>

		</Row>
	    </Container>
	)
}

