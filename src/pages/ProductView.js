
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import{Container, Card, Button, Row, Col, Form} from 'react-bootstrap';
import '../App.css';
import{useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductView() {

  const {user} = useContext(UserContext);
  // console.log(user);

  const [productName, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState ('');

  const history = useNavigate();
  const{productId} = useParams();
  

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_URL}/products/getSpecificProduct/${productId}`)
        .then(response => response.json())
        .then(data => {
                  
          setName(data.productName);
          setDescription(data.description);
          setPrice(data.price);
        })



      }, [productId])


const order = (productId) => {
console.log(productId)

  fetch(`${process.env.REACT_APP_URL}/orders/addToCart/${productId}`,{
    method: "POST",
    headers: {
      "Content-Type" : "application/json",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body : JSON.stringify({
      quantity : quantity,
    })

  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    if(data === "success"){
      console.log(data);
      Swal.fire({
        title: "Added To Cart Successfully!",
        icon: "success",
        text:"Proceed To Check Out."
      })
      history("/orders");
    }else if(data === "Admin"){
      Swal.fire({
        title: "Sorry You are an Admin!",
        icon: "error",
        text: "you are not allowed to Add product to cart."
      })
      history("/products");
    }
  }).catch(err=>{
    console.log(err)
  })
}


  return(
    <Container className="mt-5">
      <Row>
        <Col lg = {{span:6, offset: 3}}>
          <Card>
            <Card.Body className="text-center mt-5">
              <Card.Title>{productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <Form.Control
                  className="quantityform mt-3 mb-3"
                  placeholder="Enter Quantity"
                  name="mobile"
                  type="number"
                  value={quantity}
                  onChange={event => setQuantity(event.target.value)}
                  required/>
              {
              (user.id !== null)? 
              <Button variant="primary" onClick = {() => order(productId)}>Buy Now</Button>
              :
              <Button as = {Link} to = "/login" variant="primary" className = "prodviewbtn2">Log In First</Button>
              }
            </Card.Body>  
          </Card>         
        </Col>
      </Row>
    </Container>

    )
}