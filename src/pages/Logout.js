
import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Logout(){

	const {setUser, unSetUser} = useContext(UserContext);

	unSetUser();

	useEffect(() =>{
		setUser({id: null, isAdmin: false});
	},[])

	Swal.fire({
			title: "Logged Out!",
			icon: "info",
			text: ""
		})

	return(
		<Navigate to = '/login' />
	)
}