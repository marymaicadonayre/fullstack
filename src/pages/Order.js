import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';
import {Fragment, useEffect, useState, useContext} from 'react';


export default function Orders(){

const {user} = useContext(UserContext);
console.log(user)
const [orders, setOrders] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/orders/allCarts`,{
		headers: {
			"Content-Type" : "application/json",
			Authorization: `Bearer ${localStorage.getItem('token')}`
		}

		})

		.then(response => response.json())
		.then(data =>{
			console.log(data);

			setOrders(data.map(orders =>{
		return(
			<Fragment>
				<OrderCard  key = {orders._id} prop = {orders}/>
			</Fragment>
		)
	}))
		})
	},[])

	return(
		<Fragment>
			{orders}
		</Fragment>
		)
}