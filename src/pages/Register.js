import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';

//we use this to get the input of the user
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import{useNavigate} from 'react-router-dom';

import '../App.css';


export default function Register(){

//state hooks to store the values of the input field from our user
const [firstName, setFirstName]  = useState ('');
const [lastName, setLastName]  = useState ('');
const [email, setEmail] = useState ('');
const [password, setPassword] = useState ('');
const [mobileNo, setMobileNo] = useState ('');
// const [password2, setPassword2] = useState ('');
const history = useNavigate();

const [isActive, setIsActive] = useState(false);

const{user,setUser} = useContext(UserContext);

	function registerUser(event){
	event.preventDefault()

		let register = {firstName,lastName,email,password,mobileNo};
		console.log(register);

		fetch(`${process.env.REACT_APP_URL}/users/register`,{
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
			},
			body : JSON.stringify(register)
			
		})
		.then(response => response.json())
		.then(data => {

			console.log(data);
			if (data === true) {
			
			Swal.fire({
				title: "Reistered Successfully!.",
				icon: "success",
				text: "Please Login!"
			})
			history("/login");
			
			}else{

			Swal.fire({
				title: "Email Already Taken",
				icon: "error",
				text: "Please Enter Another Email"
			})
			setPassword('');		
			}
		
		})
	
	}


	return (
		(user.id !== null)?
		<Navigate to = '/' />
		:
		<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {registerUser} className = 'p-3'>

					<h2 className = "mb-4 mt-1 fw-bold">Sign Up:</h2>
					<Form.Group className="mb-3" controlId="firstName">
					    <Form.Label className= "fw-bold">First Name</Form.Label>
					    <Form.Control 
					    type = "text"
					    placeholder="Enter First Name" 
					    value = {firstName}
					    onChange = {event => setFirstName(event.target.value)}
				    	required/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="lastName">
					    <Form.Label className= "fw-bold">Last Name</Form.Label>
					    <Form.Control 
					    type = "text"
					    placeholder="Enter Last Name" 
					    value = {lastName}
					    onChange = {event => setLastName(event.target.value)}
				    	required/>
					</Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label className= "fw-bold">Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value = {email}
				    	onChange = {event => setEmail(event.target.value)}
				    	required/>
				    {/*<Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>*/}
				  </Form.Group>


				  <Form.Group className="mb-3" controlId="password1" >
				    <Form.Label className= "fw-bold">Enter your desired Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value = {password}
				    	onChange = {event => setPassword(event.target.value)}
				    	required/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="mobileNo">
				  <Form.Label className= "fw-bold">Enter your Mobile No</Form.Label>
				  <Form.Control
				      className="mobileBox"
				      placeholder="11 digit number"
				      name="mobile"
				      type="number"
				      maxLength="11"
				      value={mobileNo}
				      onChange={event => setMobileNo(event.target.value)}
				  	  required/>
				  </Form.Group>

				{/*<Form.Group className="mb-3" controlId="password2">
				    <Form.Label>Enter your desired Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value = {password2}
				    	onChange = {event => setPassword2(event.target.value)}
				    	required/>
				 </Form.Group>*/}

				  {/*disabled= {!isActive}*/}
				  <Button variant="success" type="submit"className = "registerbtn">
				    Register
				  </Button>

				</Form>
			</Col>

		</Row>
	    </Container>
	)
}

